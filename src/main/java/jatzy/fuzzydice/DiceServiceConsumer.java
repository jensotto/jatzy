package jatzy.fuzzydice;

public interface DiceServiceConsumer {
    public String throwDice(long numerOfDice) throws Exception;
}

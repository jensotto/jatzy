package jatzy.fuzzydice;

import java.util.List;

public interface TerningkastTjeneste {
    public List<Long> kastTerninger(long antall);
}

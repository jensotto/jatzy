package jatzy.fuzzydice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class DiceServiceConsumerImpl implements  DiceServiceConsumer{

    String port;

    public DiceServiceConsumerImpl(String port) {
        this.port = port;
    }

    @Override
    public String throwDice(long numberOfDice) throws Exception {

        URL url = new URL(port + "throw?n=" + String.valueOf(numberOfDice));
        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));
        StringBuilder result = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            result.append(inputLine);
        in.close();
        return result.toString();
    }
}

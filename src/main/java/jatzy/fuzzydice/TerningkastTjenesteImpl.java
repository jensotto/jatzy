package jatzy.fuzzydice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TerningkastTjenesteImpl implements TerningkastTjeneste {
    private DiceServiceConsumer diceServiceConsumer;

    public TerningkastTjenesteImpl(String port) {
        this.diceServiceConsumer = new DiceServiceConsumerImpl(port);
    }

    public TerningkastTjenesteImpl(DiceServiceConsumer diceServiceConsumer) {
        this.diceServiceConsumer = diceServiceConsumer;
    }

    @Override
    public List<Long> kastTerninger(long antall) {
        List<Long> resultat = new ArrayList<>();
        try {
            String kast = diceServiceConsumer.throwDice(antall);
            String liste = kast.substring(kast.indexOf("[") + 1, kast.indexOf("]")).replaceAll(" ", "");

            resultat.addAll(Arrays.stream(liste.split("\\s*,\\s*")).map(Long::parseLong).collect(Collectors.toList()));
        } catch (Exception e) {
            // Ignore
        }
        return resultat;
    }
}

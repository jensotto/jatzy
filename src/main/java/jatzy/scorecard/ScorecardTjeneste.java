package jatzy.scorecard;

import java.util.List;
import java.util.Optional;

public interface ScorecardTjeneste {
    public void oppdaterScorecard(Scorecard scorecard, List<Long> resultat);

    public Optional<Long> beholdTerningVerdi(Scorecard scorecard, List<Long> resultat);

    public ScorecardResultatDto lagResultat(Scorecard scorecard);
}

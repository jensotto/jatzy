package jatzy.scorecard;

import java.util.HashMap;
import java.util.Map;

public class Scorecard {
    private Map<Long, Long> resultatMap;

    public Scorecard() {
        this.resultatMap = new HashMap<>();
    }

    public Long vekt(Long prikker) {
        return resultatMap.containsKey(prikker) ? 0L : prikker;
    }

    public boolean lagreResultat(Long prikker, Long antall) {
        if (resultatMap.get(prikker) != null) return false;
        resultatMap.put(prikker, antall);
        return true;
    }

    public boolean ledig(Long prikker) {
        return !resultatMap.containsKey(prikker);
    }

    public Long antall(Long prikker) {
        return resultatMap.containsKey(prikker) ? resultatMap.get(prikker) : 0L;
    }

    public Long lavesteLedige() {
        for (long i = 1 ; i < 7; i++) if (resultatMap.get(i) == null) return i;
        return 0L;
    }

    public Long høyesteLedige() {
        for (long i = 6 ; i > 0; i--) if (resultatMap.get(i) == null) return i;
        return 0L;
    }

}

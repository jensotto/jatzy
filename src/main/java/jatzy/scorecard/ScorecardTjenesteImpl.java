package jatzy.scorecard;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ScorecardTjenesteImpl implements ScorecardTjeneste {

    public ScorecardTjenesteImpl() {
    }

    @Override
    public void oppdaterScorecard(Scorecard scorecard, List<Long> resultat) {
        // Etablere map som inneholder ledige terning-verdier
        Map<Long, Long> resultatMap = resultat.stream()
                .filter(scorecard::ledig)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Long maxAntall = resultatMap.values().isEmpty() ? 0L : Collections.max(resultatMap.values());
        // Hvis 3 eller flere fyll ut, ellers fører man laveste ledige terningverdi
        if (maxAntall > 2) {
            resultatMap.entrySet().stream().filter(e -> maxAntall.equals(e.getValue())).findAny().ifPresent(e -> {
                scorecard.lagreResultat(e.getKey(), e.getValue());
            });
        } else {
            Long lavesteLedig = scorecard.lavesteLedige();
            scorecard.lagreResultat(lavesteLedig, resultatMap.getOrDefault(lavesteLedig, 0L));
        }
    }

    @Override
    public Optional<Long> beholdTerningVerdi(Scorecard scorecard, List<Long> resultat) {
        Map<Long, Long> resultatMap = resultat.stream()
                .filter(scorecard::ledig)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // Behold terninger basert på vekting
        Long maxvekt = 0L;
        Long maxvektTerning = 0L;
        for (Map.Entry<Long, Long> entry : resultatMap.entrySet()) {
            if (maxvekt < entry.getKey() * entry.getValue()) {
                maxvekt = (entry.getKey()) * entry.getValue();
                maxvektTerning = entry.getKey();
            }
        }
        return maxvektTerning == 0L ? Optional.empty() : Optional.of(maxvektTerning);

    }

    @Override
    public ScorecardResultatDto lagResultat(Scorecard scorecard) {
        return new ScorecardResultatDto(scorecard);
    }
}

package jatzy.scorecard;

import java.util.HashMap;
import java.util.Map;

public class ScorecardResultatDto {
    private Map<Long, Long> scorecard;
    private Long total = 0L;

    public ScorecardResultatDto() {}

    public ScorecardResultatDto(Scorecard scorecard) {
        this.scorecard = new HashMap<>();
        for (long i = 1 ; i < 7 ; i++) {
            this.scorecard.put(i, scorecard.antall(i));
            this.total += i * scorecard.antall(i);
        }
    }

    public Map<Long, Long> getScorecard() { return scorecard; }

    public Long getTotal() { return total; }

    @Override
    public String toString() {
        return "Scorecard=" + scorecard.toString() + "\nTotalt=" + total.toString();
    }
}

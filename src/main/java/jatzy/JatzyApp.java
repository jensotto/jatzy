package jatzy;

import jatzy.fuzzydice.TerningkastTjeneste;
import jatzy.fuzzydice.TerningkastTjenesteImpl;
import jatzy.scorecard.Scorecard;
import jatzy.scorecard.ScorecardResultatDto;
import jatzy.scorecard.ScorecardTjeneste;
import jatzy.scorecard.ScorecardTjenesteImpl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Yatzy-applikasjon med enkel test. Har sett resultat opp til 80.
 * Strategi: Behold kast med 3+ terninger, ellers se på vekting og stryk innslag med lavest potensiale
 * Illustrerer noen patterns og ivrig bruk av Java 8.
 */
public class JatzyApp
{
    private String diceUrl = "https://nowhere";
    private ScorecardTjeneste scorecardTjeneste;
    private TerningkastTjeneste terningkastTjeneste;

    public JatzyApp() {
        configure();
        scorecardTjeneste = new ScorecardTjenesteImpl();
        terningkastTjeneste = new TerningkastTjenesteImpl(diceUrl);
    }

    // For å mocke tjenestekall i testformål
    public JatzyApp(TerningkastTjeneste terningkastTjeneste) {
        configure();
        scorecardTjeneste = new ScorecardTjenesteImpl();
        this.terningkastTjeneste = terningkastTjeneste;
    }

    public static void main( String[] args )
    {
        JatzyApp jatzyApp = new JatzyApp();
        jatzyApp.spillJatzy();
    }

    public Scorecard spillJatzy() {
        System.out.println( "jatzeeee!" );
        Scorecard scorecard = new Scorecard();

        for (int i = 0 ; i < 6; i++) kastOmgang(scorecard);

        ScorecardResultatDto resultatDto = scorecardTjeneste.lagResultat(scorecard);
        System.out.println( resultatDto.toString() );
        return scorecard;
    }

    private void kastOmgang(Scorecard scorecard) {
        List<Long> resultat = kastTerning(scorecard, 3L, new ArrayList<>());
        scorecardTjeneste.oppdaterScorecard(scorecard, resultat);
    }

    private List<Long> kastTerning(Scorecard scorecard, Long restKast, List<Long> mente) {
        restKast--;
        List<Long> resultat = terningkastTjeneste.kastTerninger(5L - mente.size());
        resultat.addAll(mente);
        // siste kast. overlat evaluering til kastOmgang
        if (restKast == 0L) return resultat;

        List<Long> nyMente = new ArrayList<>();
        scorecardTjeneste.beholdTerningVerdi(scorecard, resultat).ifPresent(behold -> {
            nyMente.addAll(resultat.stream().filter(behold::equals).collect(Collectors.toList()));
        });
        
        return kastTerning(scorecard, restKast, nyMente);
    }

    // Les inn property-fil med URL
    private void configure () {
        Properties props = new Properties();

        InputStream res = this.getClass().getResourceAsStream("jatzy.properties");
        if (res != null) {
            try {
                props.load(res);
                if (props.getProperty("Dices_url") != null) {
                    this.diceUrl = props.getProperty("Dices_url");
                }
            } catch (Exception e) {
                // ignore
            }
        }
        if (!diceUrl.endsWith("/")) {
            diceUrl = diceUrl + "/";
        }
    }

}

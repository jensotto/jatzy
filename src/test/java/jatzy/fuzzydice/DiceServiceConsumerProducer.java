package jatzy.fuzzydice;


import jatzy.fuzzydice.DiceServiceConsumer;

public class DiceServiceConsumerProducer implements DiceServiceConsumer {

    String port;

    public DiceServiceConsumerProducer() {}

    public DiceServiceConsumerProducer(String port) {
        this.port = port;
    }

    @Override
    public String throwDice(long numberOfDice) throws Exception {
        if (numberOfDice == 5) return "[ 1, 3, 6, 2, 3]";
        if (numberOfDice == 4) return "[ 2, 2, 4, 6]";
        if (numberOfDice == 3) return "[ 1, 3, 5 ]";
        if (numberOfDice == 2) return "[ 2, 5 ]";
        return "[ 4 ]";
    }
}

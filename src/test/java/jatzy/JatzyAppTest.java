package jatzy;

import jatzy.fuzzydice.DiceServiceConsumerProducer;
import jatzy.fuzzydice.TerningkastTjeneste;
import jatzy.fuzzydice.TerningkastTjenesteImpl;
import jatzy.scorecard.Scorecard;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple JatzyApp.
 */
public class JatzyAppTest
{

    /**
     * Rigorous Test :-)
     */
    @Test
    public void tolkerListeRiktig()
    {
        TerningkastTjeneste terningkastTjeneste = new TerningkastTjenesteImpl(new DiceServiceConsumerProducer());
        List<Long> resultat = terningkastTjeneste.kastTerninger(5L);

        assertTrue(resultat.size() == 5L);
    }

    @Test
    public void alleScorecardsFyltUt()
    {
        JatzyApp jatzyApp = new JatzyApp(new TerningkastTjenesteImpl(new DiceServiceConsumerProducer()));
        Scorecard scorecard = jatzyApp.spillJatzy();

        assertTrue(scorecard.høyesteLedige().equals(0L));
    }
}
